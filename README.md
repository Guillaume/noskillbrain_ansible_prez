# Noskillbrain: projet Ansible

Projet de présentation Ansible du blog noskillbrain.fr, article https://noskillbrain.fr/2017/10/18/ansible/

## Prérequis

* Ansible >= 2.4.0
* https://noskillbrain.fr/2017/10/18/ansible/

## Utilisation

```
ansible-playbook -i mon_inventaire configuration.yml -v
```

## Contributeurs

 - [Guillaume Tredez](https://noskillbrain.fr) - [Twitter: @noskillbrain](https://twitter.com/noskillbrain)
